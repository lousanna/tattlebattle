Lousanna, Jeffrey, Dylan, Piyush

For HopHacks:
A mobile app to gamify the moderation of social media. 
Tweets from a user's newsfeed can be classified as online harassment
via crowdsourcing, and users are then given points which they can redeem for 
real rewards.
