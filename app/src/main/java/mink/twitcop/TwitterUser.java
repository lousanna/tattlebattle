package mink.twitcop;

public class TwitterUser {
    public String username;
    public String email;

    public TwitterUser(String username, String email) {
        this.username = username;
        this.email = email;
    }

}