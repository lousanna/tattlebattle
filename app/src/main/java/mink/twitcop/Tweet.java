package mink.twitcop;

public class Tweet {
    public String username;
    public String message;
    public String image_url;
    private double semantic;
    public Tweet(String username, String message, String url) {
        this.username = username;
        this.message = message;
        this.image_url = url;
        semantic = 1.0;
    }
}