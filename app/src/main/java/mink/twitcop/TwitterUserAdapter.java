package mink.twitcop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import static mink.twitcop.R.styleable.View;

public class TwitterUserAdapter extends ArrayAdapter<TwitterUser> {
    private ArrayList<TwitterUser> users;
    public TwitterUserAdapter(Context context,
                              int textViewResourceId,
                              ArrayList<TwitterUser> users) {
        super(context, textViewResourceId, users);
        this.users = users;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null ) {
            LayoutInflater vi = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.tweetcard, null);
        }
        TwitterUser user = users.get(position);
        if (user != null) {
            TextView username = (TextView) v.findViewById(R.id.username);
            TextView message = (TextView) v.findViewById(R.id.message);

            if (username != null)
                username.setText(user.username);
            if (message != null)
                message.setText(user.email);
        }
        return v;
    }
}

