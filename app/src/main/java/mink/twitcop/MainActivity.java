package mink.twitcop;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.net.URLConnection;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.StrictMode;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Button;
import android.widget.TextView;
import com.wdullaer.swipeactionadapter.SwipeActionAdapter;
import com.wdullaer.swipeactionadapter.SwipeDirection;
import android.widget.Toast;

import twitter4j.*;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

import com.reimaginebanking.api.nessieandroidsdk.requestclients.*;
import com.reimaginebanking.api.nessieandroidsdk.models.*;
import com.reimaginebanking.api.nessieandroidsdk.constants.*;
import com.reimaginebanking.api.nessieandroidsdk.NessieError;
import com.reimaginebanking.api.nessieandroidsdk.NessieResultsListener;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import java.io.OutputStreamWriter;
import java.io.InputStream;
import android.os.AsyncTask;
import java.io.PrintWriter;
import java.lang.*;
import java.io.InputStreamReader;
import java.io.BufferedReader;

public class MainActivity extends AppCompatActivity implements
        SwipeActionAdapter.SwipeActionListener{
    public Customer curCust;
    public Account curAcc;
    static public String custid = "";
    static public String accid = "";
    public DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public Date date = new Date();
    public NessieClient client = NessieClient.getInstance("833fe835a92df17e2ead9ae46b914877");
    protected SwipeActionAdapter mAdapter;

    public ArrayList<Tweet> finalStats;
    public ListView tweetList;
    public ArrayList<Tweet> harassment = new ArrayList<>();

    static String TWITTER_CONSUMER_KEY = "pffoMpFDEMWdDll7Sx2hh17YU";
    static String TWITTER_CONSUMER_SECRET = "ohkkevIeeDHAVLi6583RV7mIfTn2eRf5RFuxAIqkOzpbwitPFN";

    // Preference Constants
    static String PREFERENCE_NAME = "twitter_oauth";
    static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
    static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
    static final String PREF_KEY_TWITTER_LOGIN = "isTwitterLoggedIn";

    static final String TWITTER_CALLBACK_URL = "twitcop-oauth-twitter://callback";

    // Twitter oauth urls
    static final String URL_TWITTER_AUTH = "auth_url";
    static final String URL_TWITTER_OAUTH_VERIFIER = "oauth_verifier";
    static final String URL_TWITTER_OAUTH_TOKEN = "oauth_token";

    // Login button
    Button btnLoginTwitter;


    // Twitter
    private static Twitter twitter;
    private static RequestToken requestToken;

    // Shared Preferences
    private static SharedPreferences mSharedPreferences;

    // Internet Connection detector
    private ConnectionDetector cd;

    // Alert Dialog Manager
    AlertDialogManager alert = new AlertDialogManager();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.tweetlist);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        Intent intent = new Intent(this, LoginActivity.class);
        startActivityForResult(intent, 2);
    }

    public void loadTweets() {
        List<Status> statuses = null;
        int i = 0;


        try {
            statuses = twitter.getHomeTimeline();
        } catch (TwitterException e) {
            e.printStackTrace();
        }
        finalStats = new ArrayList<>(statuses.size());

        for (Status status : statuses) {
            Tweet tweet = new Tweet(status.getUser().getName(), status.getText(), status.getUser().getProfileImageURL());
            finalStats.add(tweet);
            i++;
        }

        tweetList = (ListView) findViewById(R.id.TweetList);
        mAdapter = new SwipeActionAdapter(new TweetAdapter(this, android.R.layout.simple_list_item_1, finalStats));
        mAdapter.addBackground(SwipeDirection.DIRECTION_FAR_LEFT,R.layout.row_bg_left_far)
                .addBackground(SwipeDirection.DIRECTION_NORMAL_LEFT,R.layout.row_bg_left)
                .addBackground(SwipeDirection.DIRECTION_FAR_RIGHT,R.layout.row_bg_right_far)
                .addBackground(SwipeDirection.DIRECTION_NORMAL_RIGHT,R.layout.row_bg_right);
        mAdapter.setSwipeActionListener(this)
                .setDimBackgrounds(true)
                .setListView(tweetList);
        tweetList.setAdapter(mAdapter);


        String[] options = {"Home", "Redeem", "Logout"};
        final ListView navList = (ListView) findViewById(R.id.navList);
        navList.setAdapter(new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, options));
        navList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
                mDrawerLayout.closeDrawer(Gravity.LEFT);
                if (position == 1) {
                    Intent intent = new Intent(MainActivity.this, RewardManager.class);
                    intent.putExtra("Account", accid);
                    startActivityForResult(intent, 1);
                } else if (position == 2)
                    logoutFromTwitter();
            }
        });

    }

    public boolean hasActions(int position, SwipeDirection direction) {
        if (direction.isLeft()) return true; // Change this to false to disable left swipes
        if (direction.isRight()) return true;
        return false;
    }

    @Override
    public boolean shouldDismiss(int position, SwipeDirection direction){
        // Only dismiss an item when swiping normal left
        return direction == SwipeDirection.DIRECTION_NORMAL_RIGHT ||
                direction == SwipeDirection.DIRECTION_FAR_RIGHT;
    }
    @Override
    public void onSwipe(int[] positionList, SwipeDirection[] directionList) {
        for (int i = 0; i < positionList.length; i++) {
            SwipeDirection direction = directionList[i];
            int position = positionList[i];
            if (shouldDismiss(position, direction)) {
                harassment.add((Tweet) mAdapter.getItem(position));
                finalStats.remove(mAdapter.getItem(position));
                Deposit deposit = new Deposit.Builder()
                        .amount(100)
                        .description("Marked a tweet as harassment.")
                        .transactionDate(dateFormat.format(date))
                        .medium(TransactionMedium.BALANCE)
                        .build();
                client.DEPOSIT.createDeposit(curAcc.getId(), deposit, new NessieResultsListener() {
                    @Override
                    public void onSuccess(Object result) {
                        PostResponse<Deposit> response = (PostResponse<Deposit>) result;
                        Deposit depositRet = response.getObjectCreated();
                        System.out.println(response.getMessage());
                    }
                    @Override
                    public void onFailure(NessieError error) {
                        System.out.println(error.getCode()+" "+error.getMessage());
                    }

                });
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle("Harassment Reported");
                alertDialog.setMessage("You just earned 100 coins!");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            } else {
                finalStats.remove(mAdapter.getItem(position));
                Toast.makeText(this, "Marked as safe.",
                        Toast.LENGTH_SHORT).show();
            }
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                logoutFromTwitter();
            }

            if (resultCode == RESULT_CANCELED) {
            }
        } else if (requestCode == 2){
            if (resultCode == RESULT_OK) {
                mSharedPreferences = getApplicationContext().getSharedPreferences(
                        "MyPref", 0);
                //logoutFromTwitter();


                ConfigurationBuilder cb = new ConfigurationBuilder();
                cb.setDebugEnabled(true)
                        .setOAuthConsumerKey(TWITTER_CONSUMER_KEY)
                        .setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET)
                        .setOAuthAccessToken(mSharedPreferences.getString(PREF_KEY_OAUTH_TOKEN, null))
                        .setOAuthAccessTokenSecret(mSharedPreferences.getString(PREF_KEY_OAUTH_SECRET, null));
                TwitterFactory tf = new TwitterFactory(cb.build());

                twitter = tf.getInstance();
                loadTweets();

                // Check webserver for existing user account.
                String id = checkUser();
                if (id != null && id.equals("")) {
                    saveUser();
                } else accid = id;
            }
            if (resultCode == RESULT_CANCELED) {
            }

        }
    }

    private String checkUser() {
        final String str = new String();

        AsyncTask<Void,Void,String> checkUserinDB = new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... params) {
                String output = getOutputFromUrl("http://10.0.2.2:8080/TwitCop/Form?");

                return output;
            }
            private String getOutputFromUrl(String url) {
                StringBuffer output = new StringBuffer("");
                try {
                    InputStream stream = getHttpConnection(url);
                    BufferedReader buffer = new BufferedReader(new InputStreamReader(stream));
                    String s = "";
                    while ((s = buffer.readLine()) != null){
                        output.append(s);
                    }

                } catch (Exception e) {
                }
                return output.toString();
            }
                    // Makes HttpURLConnection and returns InputStream
            private InputStream getHttpConnection(String urlString) throws Exception {
                    InputStream stream = null;
                    URL url = new URL(urlString);

                try {
                    String toSend = "findtwit=" + twitter.getScreenName();
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    connection.setDoOutput(true);
                    connection.setRequestMethod("GET");

                    PrintWriter pw = new PrintWriter(connection.getOutputStream());
                    pw.print(twitter.getScreenName());
                    pw.close();
                    if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        stream = connection.getInputStream();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return stream;
            }
        };
        checkUserinDB.execute();
        return str;
    }

    private void saveUser() {
        Address add = new Address.Builder()
                .streetNumber("1")
                .streetName("Fake St.")
                .city("College Park")
                .state("MD")
                .zip("20874")
                .build();

        Customer customer = null;
        try {
            customer = new Customer.Builder()
                    .firstName(twitter.getScreenName()+"")
                    .lastName("NA")
                    .address(add)
                    .build();
        } catch (TwitterException e) {
            e.printStackTrace();
        }

        client.CUSTOMER.createCustomer(customer, new NessieResultsListener() {
            @Override
            public void onSuccess(Object result) {
                PostResponse<Customer> response = (PostResponse<Customer>) result;
                custid = custid.concat(response.getObjectCreated().getId());
                curCust = response.getObjectCreated();
                System.out.println("Customer created.");
                Account acc = new Account.Builder()
                        .type(AccountType.CHECKING)
                        .nickname("Rewards")
                        .rewards(0)
                        .balance(0)
                        .build();
                client.ACCOUNT.createAccount(custid, acc, new NessieResultsListener() {
                    @Override
                    public void onSuccess(Object result) {
                        PostResponse<Account> response = (PostResponse<Account>) result;
                        curAcc = response.getObjectCreated();
                        accid = accid.concat(response.getObjectCreated().getId());
                        System.out.println("Account created. ID: "+response.getObjectCreated().getId());
                        AsyncTask<Void,Void,String> sendUsertoDB = new AsyncTask<Void, Void, String>() {
                            @Override
                            protected String doInBackground(Void... params) {
                                URL url = null;
                                try {
                                    url = new URL("http://10.0.2.2:8080/TwitCop/Form");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                String text;
                                text = null;
                                try {
                                    if (accid == "") {
                                        accid =checkUser();
                                    }
                                    String toSend = twitter.getScreenName() + "&" + accid;
                                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                                    connection.setRequestProperty("Accept-Charset", "UTF-8");
                                    connection.setRequestProperty("User-Agent", "Mozilla/5.0 ( compatible ) ");
                                    connection.setRequestProperty("Accept", "*/*");
                                    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                                    connection.setFixedLengthStreamingMode(toSend.getBytes().length);
                                    connection.setDoOutput(true);
                                    connection.setRequestMethod("POST");
                                    Log.d("sendName", toSend);
                                    PrintWriter pw = new PrintWriter(connection.getOutputStream());
                                    pw.print(toSend);
                                    pw.close();
                                    Log.d("response",connection.getResponseMessage());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                return text;
                            }
                        };
                        sendUsertoDB.execute();
                    }
                    @Override
                    public void onFailure(NessieError error) {
                        System.out.println(error.getCode()+" "+error.getMessage());

                    }
                });
            }
            @Override
            public void onFailure(NessieError error) {
                System.out.println(error.getCode()+" "+error.getMessage());
            }

        });
    }
    private void logoutFromTwitter() {
        // Clear the shared preferences
        SharedPreferences.Editor e = mSharedPreferences.edit();
        e.remove(PREF_KEY_OAUTH_TOKEN);
        e.remove(PREF_KEY_OAUTH_SECRET);
        e.remove(PREF_KEY_TWITTER_LOGIN);
        e.commit();

        twitter = null;
        requestToken = null;

        Intent intent = new Intent(this, LoginActivity.class);
        startActivityForResult(intent, 2);
    }
}