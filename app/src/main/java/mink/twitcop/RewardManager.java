package mink.twitcop;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.reimaginebanking.api.nessieandroidsdk.NessieError;
import com.reimaginebanking.api.nessieandroidsdk.NessieResultsListener;
import com.reimaginebanking.api.nessieandroidsdk.constants.TransactionMedium;
import com.reimaginebanking.api.nessieandroidsdk.models.Account;
import com.reimaginebanking.api.nessieandroidsdk.models.PostResponse;
import com.reimaginebanking.api.nessieandroidsdk.models.Withdrawal;
import com.reimaginebanking.api.nessieandroidsdk.requestclients.NessieClient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RewardManager extends AppCompatActivity implements
        ListAdapter.customButtonListener {

    public ListAdapter adapter;
    public String accid;
    public NessieClient client = NessieClient.getInstance("833fe835a92df17e2ead9ae46b914877");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rewards);
        String[] options = {"Home", "Redeem", "Logout"};
        final ListView navList = (ListView) findViewById(R.id.navList);
        navList.setAdapter(new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, options));

        navList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    Intent resultIntent = new Intent();
                    setResult(RESULT_CANCELED, resultIntent);
                    finish();
                } else if (position == 2) {
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("result", "Logout");
                    setResult(RESULT_OK, resultIntent);
                    finish();
                }
            }
        });
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            accid = extras.getString("Account");
        }
        ArrayList<String> options2 = new ArrayList<String>();
        options2.add("Amazon 10$ Gift Card");
        options2.add("Google 10$ Gift Card");
        options2.add("Paypal 10$ Gift Card");
        ListView rewardsList = (ListView) findViewById(R.id.rewardsList);
        adapter = new ListAdapter(RewardManager.this, options2);
        adapter.setCustomButtonListener(RewardManager.this);
        rewardsList.setAdapter(adapter);
    }

    public void onStart() {
        super.onStart();
        updateBalance();
    }

    public void updateBalance() {
        client.ACCOUNT.getAccount(accid, new NessieResultsListener() {

            @Override
            public void onSuccess(Object result) {
                //PostResponse<Account> response = (PostResponse<Account>) result;
                Account acc = (Account) result;
                TextView tv = (TextView) findViewById(R.id.coins);
                tv.setText(acc.getBalance() + " Coins");

            }

            @Override
            public void onFailure(NessieError error) {
                System.out.println(error.getCode() + " " + error.getMessage());
            }
        });
    }

    public void onButtonClickListener(int position, String value) {
        client.ACCOUNT.getAccount(accid, new NessieResultsListener() {

            @Override
            public void onSuccess(Object result) {
                //PostResponse<Account> response = (PostResponse<Account>) result;
                Account acc = (Account) result;
                TextView tv = (TextView) findViewById(R.id.coins);
                tv.setText(acc.getBalance() + " Coins");

            }

            @Override
            public void onFailure(NessieError error) {
                System.out.println(error.getCode() + " " + error.getMessage());
            }
        });
        Toast.makeText(RewardManager.this, "Claimed " + value,
                Toast.LENGTH_SHORT).show();
        Withdrawal withdrawal = new Withdrawal.Builder()
                .amount(1000)
                .description("Claimed a reward.")
                .medium(TransactionMedium.BALANCE)
                .build();
        client.WITHDRAWAL.createWithdrawal(accid, withdrawal, new NessieResultsListener() {
            @Override
            public void onSuccess(Object result) {
                PostResponse<Withdrawal> response = (PostResponse<Withdrawal>) result;
                Withdrawal withdrawlRet = response.getObjectCreated();
                System.out.println(response.getMessage());
                updateBalance();
            }

            @Override
            public void onFailure(NessieError error) {
                System.out.println(error.getCode() + " " + error.getMessage());
            }

        });
    }
}
